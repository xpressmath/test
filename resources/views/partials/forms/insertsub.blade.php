@extends('layouts.page')
@section('pagecontent')
<h1>Insert Items</h1>
<p class="lead">Add to your Main Menu</p>
<div class="container">
  <form method="post" action="{{url('/admin/insert-mvsubtopic')}}">
    <div class="form-group row">
      {{csrf_field()}}
      <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">SubTopic Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Enter Topic Name" name="subtopicname">
      </div>
            <div class="col-sm-10">




       <select class="form-control" id="sel1" name="mvtopic-select">

@foreach($topics as $topic)
         <option value="{{$topic->topicid}}">{{$topic->topicname}}</option>
    @endforeach}}
  </select>
  </div>
    </div>
    <div class="form-group row">
      <div class="col-md-2"></div>
      <input type="submit" class="btn btn-primary">
    </div>
  </form>
</div>
@stop
