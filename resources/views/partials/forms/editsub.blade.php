@extends('layouts.page')
@section('pagecontent')
<h1>Edit</h1>
<p class="lead">Add to your Main Menu</p>
<div class="container">
  <form method="post" action="{{action('CRUDSUBController@update', $subtopicid)}}">
    <div class="form-group row">
      {{csrf_field()}}
       <input name="_method" type="hidden" value="PATCH">
      <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">topicname</label>
      <div class="col-sm-10">
        <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title" name="title" value="{{$mvsubtopic->subtopicname}}">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-md-2"></div>
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </form>
</div>
@stop
