    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel Multi Auth Guard') }}: Mathsvis
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->

                    @foreach ($topics1 as $topic)
                    <li class="dropdown">
                    <a href="/page/{{$topic->topicid}}" data-toggle="dropdown" class="dropdown-toggle">{{$topic->topicname}} 
                    <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                    @foreach ($subtopics1 as $subtopic)
                    @if($subtopic->topicid == $topic->topicid)
                    <li class="dropdown-submenu subsubmenu"><a href="/page/{{$topic->topicid}}/{{$subtopic->subtopicid}}" data-toggle="dropdown" class="dropdown-toggle">{{$subtopic->subtopicname}}</a>
                    <ul class="dropdown-menu subsub">
                    @foreach ($subsubtopics1 as $subsubtopic)
                    @if($subsubtopic->subtopicid == $subtopic->subtopicid)
                    <li><a href="/page/{{$topic->topicid}}/{{$subtopic->subtopicid}}/{{$subsubtopic->subsubtopicid}}">{{$subsubtopic->subsubtopicname}}</a></li>
                    @endif
                    @endforeach
                    </ul>
                    </li>
                    @endif
                    @endforeach
                    </ul>   
                    </li>
                    @endforeach


                    @if (Auth::guest())
                        <li><a href="{{ url('/customer/login') }}">Login</a></li>
                        <li><a href="{{ url('/customer/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/customer/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/customer/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>