@extends('layouts.page')
@section('pagecontent')
<div class="container">
    <div class="row">
       
 @foreach($mvsubsubtopics as $post)
<li>
<a href="/admin/create">{{$post['subsubtopicid']}}{{$post['subsubtopicname']}}
</a>
<a href="{{action('CRUDSUBSUBController@edit', $post['subsubtopicid'])}}" class="btn btn-warning">Edit</a>
 <form action="{{action('CRUDSUBSUBController@destroy', $post['subsubtopicid'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
</li>
      @endforeach   
    </div>
</div>
 
@stop