@extends('admin.layout.auth')
@section('content')

<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-4">
      <a href="/admin/insert-mvtopic/create" class="btn btn-primary btn-block text-center">Insert Main Menu Item</a>
    </div>
    <div class="col-xs-12 col-sm-4">
      <a href="/admin/insert-mvsubtopic/create" class="btn btn-primary btn-block text-center">Insert Sub Menu Item</a>
    </div>
    <div class="col-xs-12 col-sm-4">
      <a href="/admin/insert-mvsubsubtopic/create" class="btn btn-primary btn-block text-center">Insert Sub-Sub Menu Item</a>
    </div>

</div>
</div>
<br><br>


<div class="container">
    <div class="row">


<ul id="tree3">
@foreach($mvtopics as $mvtopic)
<li>
{{$mvtopic['topicname']}}
<button onclick="location.href='{{action('CRUDController@edit', $mvtopic['topicid'])}}'" type="button"><i class="fa fa-pencil
-square-o" aria-hidden="true"></i></button>
 <form action="{{action('CRUDController@destroy', $mvtopic['topicid'])}}" method="post"  style="display:inline">
 {{csrf_field()}}
<input name="_method" type="hidden" value="DELETE">
<button type="submit" style="display:inline"><i class="fa fa-trash-o" aria-hidden="true"></i>
</button>
          </form> 

<ul>
@foreach($mvsubtopics as $mvsubtopic)
@if($mvsubtopic['topicid'] == $mvtopic['topicid'])
<li>
{{$mvsubtopic['subtopicname']}}

<button onclick="location.href='{{action('CRUDSUBController@edit', $mvsubtopic['subtopicid'])}}'" type="button"><i class="fa fa-pencil
-square-o" aria-hidden="true"></i></button>
 <form action="{{action('CRUDSUBController@destroy', $mvsubtopic['subtopicid'])}}" method="post"  style="display:inline">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit" style="display:inline"><i class="fa fa-trash-o" aria-hidden="true"></i>
</button>
          </form> 


<ul>

 @foreach($mvsubsubtopics as $mvsubsubtopic)
 @if($mvsubsubtopic['subtopicid'] == $mvsubtopic['subtopicid'])

<li>
{{$mvsubsubtopic['subsubtopicname']}}
<button onclick="location.href='{{action('CRUDSUBSUBController@edit', $mvsubsubtopic['subsubtopicid'])}}'" type="button"><i class="fa fa-pencil
-square-o" aria-hidden="true"></i></button>
 <form action="{{action('CRUDSUBSUBController@destroy', $mvsubsubtopic['subsubtopicid'])}}" method="post"  style="display:inline">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button type="submit" style="display:inline"><i class="fa fa-trash-o" aria-hidden="true"></i>
</button>
          </form> 
</li>
@endif
    @endforeach   

</ul>
</li>
@endif
    @endforeach 
</ul>
</li>
    @endforeach 
</ul>
</div>
</div>
 





















<!--  <div class="container" style="margin-top:30px;">
    <div class="row">


        <div class="col-md-4">

            <ul id="tree3">
                <li><a href="#">TECH</a>

                    <ul>
                        <li>Company Maintenance</li>
                        <li>Employees
                            <ul>
                                <li>Reports
                                    <ul>
                                        <li>Report1</li>
                                        <li>Report2</li>
                                        <li>Report3</li>
                                    </ul>
                                </li>
                                <li>Employee Maint.</li>
                            </ul>
                        </li>
                        <li>Human Resources</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div> -->
@stop