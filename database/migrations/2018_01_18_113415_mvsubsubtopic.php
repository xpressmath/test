<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mvsubsubtopic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('mvsubsubtopics', function (Blueprint $table) 
        {
            $table->increments('subsubtopicid');
            $table->integer('subtopicid');
            $table->integer('topicid');
            $table->string('subsubtopicname',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mvsubsubtopics');
    }
}
