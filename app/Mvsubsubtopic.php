<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvsubsubtopic extends Model
{
    protected $primaryKey = 'subsubtopicid';
    protected $fillable = ['subtopicid','topicid','subsubtopicname'];
     public function Mvsubtopic() 
    {
    	return $this->hasMany('App\Mvsubtopic');
	}
	 public function Mvtopic() 
    {
    	return $this->hasMany('App\Mvtopic');
	}
}
