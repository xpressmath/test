<?php

namespace App\Providers;

use App\Mvtopic;
use App\Mvsubtopic;
use App\Mvsubsubtopic;
use Illuminate\Support\ServiceProvider;
use  Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer('customer.nav', function($view)
        {
            $view->with('topics1', Mvtopic::all());
            $view->with('subtopics1', Mvsubtopic::all());
            $view->with('subsubtopics1', Mvsubsubtopic::all());
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
        $this->app->register('Hesto\MultiAuth\MultiAuthServiceProvider');
    }
    }
}
