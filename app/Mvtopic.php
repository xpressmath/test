<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvtopic extends Model
{
	protected $primaryKey = 'topicid';
    protected $fillable = ['topicname'];

    public function Mvsubtopic() 
    {
    	return $this->belongsTo('App\Mvsubtopic');
	}
	 public function Mvsubsubtopic() 
    {
    	return $this->belongsTo('App\Mvsubsubtopic');
	}

}
