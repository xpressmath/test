<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mvsubtopic extends Model
{
    protected $primaryKey = 'subtopicid';
    protected $fillable = ['subtopicname','topicid'];
     public function Mvtopic() 
    {
    	return $this->hasMany('App\Mvtopic');
	}
	 public function Mvsubsubtopic() 
    {
    	return $this->belongsTo('App\Mvsubsubtopic');
	}
}
