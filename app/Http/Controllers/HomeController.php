<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mvtopic;
use App\Mvsubtopic;
use App\Mvsubsubtopic;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*    public function __construct()
    {
        $this->middleware('auth');
    }*/

   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_index()
    {
      return view('customer.main');
    }

    
        public function customer_show1($topicid)
    {
      $item1=Mvtopic::find($topicid);
      $item2='';
      $item3s='';
      if(is_null($item1))
      {
        abort(404);
      }
      return view('customer.page', compact('item1','item2','item3'));
    }

  public function customer_show2($topicid,$subtopicid)
    {
      $item1='';
      $item2=Mvsubtopic::find($subtopicid);
      $item3='';
      if(is_null($item2))
      {
        abort(404);
      }
      return view('customer.page', compact('item1','item2','item3'));
    }
      public function customer_show3($topicid,$subtopicid,$subsubtopicid)
    {
      $item1='';
      $item2='';
      $item3=Mvsubsubtopic::find($subsubtopicid);
      if(is_null($item3))
      {
        abort(404);
      }
      return view('customer.page', compact('item1','item2','item3'));
    }
    public function admin_index()
    {
      return view('admin.dashboard');
    }
}
