<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mvtopic;
use App\Mvsubtopic;
use App\Mvsubsubtopic;

class CRUDSUBSUBController extends Controller
{
     public function index()
    {
        $mvtopics = Mvtopic::all()->toArray();
          $mvsubtopics = Mvsubtopic::all()->toArray();
          $mvsubsubtopics = Mvsubsubtopic::all()->toArray();
          return view('admin.mvtopic', compact('mvtopics','mvsubtopics','mvsubsubtopics'));
    }



     public function create()
    {
    	$topics = Mvtopic::all();
    	$sbtopics = Mvsubtopic::all();
        return view('partials.forms.insertsubsub',compact('topics','sbtopics'));
    }							


     public function store(Request $request)
    {
         $mvsubsubtopic = new Mvsubsubtopic([
          'subsubtopicname' => $request->input('subsubtopicname'),
         'topicid' => $request->input('mvtopic-select'),
         'subtopicid' => $request->input('mvsubtopic-select')
        ]);
        $mvsubsubtopic->save();
        return redirect('/admin/insert-mvtopic');
    }



    public function show($id)
    {
        //
    }


    public function edit($subsubtopicid)
    {
        $mvsubsubtopic = Mvsubsubtopic::find($subsubtopicid);
        return view('partials.forms.editsubsub', compact('mvsubsubtopic','subsubtopicid'));
    }

     public function update(Request $request, $subsubtopicid)
    {
        $mvsubsubtopic = Mvsubsubtopic::find($subsubtopicid);
        $mvsubsubtopic->subsubtopicname = $request->get('title');
        $mvsubsubtopic->save();
        return redirect('/admin/insert-mvsubsubtopic/');
    }

     public function destroy($topicid)
    {
        $mvsubsubtopic = Mvsubsubtopic::find($subsubtopicid);
        $mvsubsubtopic->delete();
        return redirect('/admin/mvsubsubtopic/');
    }
}
