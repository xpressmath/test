<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mvtopic;
use App\Mvsubtopic;
use App\Mvsubsubtopic;


class CRUDController extends Controller
{
  

	 public function index()
    {
          $mvtopics = Mvtopic::all()->toArray();
          $mvsubtopics = Mvsubtopic::all()->toArray();
          $mvsubsubtopics = Mvsubsubtopic::all()->toArray();
          return view('admin.mvtopic', compact('mvtopics','mvsubtopics','mvsubsubtopics'));
    }



     public function create()
    {
        return view('partials.forms.insert');
    }


     public function store(Request $request)
    {
         $mvtopic = new Mvtopic([
          'topicname' => $request->get('topicname')
        ]);
        $mvtopic->save();
        return redirect('/admin/insert-mvtopic');
    }



    public function show($id)
    {
        //
    }


    public function edit($topicid)
    {
        $mvtopic = Mvtopic::find($topicid);
        return view('partials.forms.edit', compact('mvtopic','topicid'));
    }

     public function update(Request $request, $topicid)
    {
        $mvtopic = Mvtopic::find($topicid);
        $mvtopic->topicname = $request->get('title');
        $mvtopic->save();
        return redirect('/admin/insert-mvtopic/');
    }

     public function destroy($topicid)
    {
        $mvtopic = Mvtopic::find($topicid);
        $mvtopic->delete();
        return redirect('/admin/insert-mvtopic/');
    }
}
