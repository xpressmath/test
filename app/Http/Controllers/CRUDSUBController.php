<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mvtopic;
use App\Mvsubtopic;

class CRUDSUBController extends Controller
{
 	 public function index()
    {
        $mvsubtopics = Mvsubtopic::all()->toArray();
        return view('admin.mvtopic', compact('mvsubtopics'));
    }



     public function create()
    {
    	$topics = Mvtopic::all();
        return view('partials.forms.insertsub',compact('topics'));
    }							


     public function store(Request $request)
    {
         $mvsubtopic = new Mvsubtopic([
          'subtopicname' => $request->input('subtopicname'),
          'topicid' => $request->input('mvtopic-select')
        ]);
        $mvsubtopic->save();
        return redirect('/admin/insert-mvtopic');
    }



    public function show($id)
    {
        //
    }


    public function edit($subtopicid)
    {
        $mvsubtopic = Mvsubtopic::find($subtopicid);
        return view('partials.forms.editsub', compact('mvsubtopic','subtopicid'));
    }

     public function update(Request $request, $subtopicid)
    {
        $mvsubtopic = Mvsubtopic::find($subtopicid);
        $mvsubtopic->subtopicname = $request->get('title');
        $mvsubtopic->save();
        return redirect('/admin/insert-mvsubsubtopic/');
    }

     public function destroy($subtopicid)
    {
        $mvsubtopic = Mvsubtopic::find($subtopicid);
        $mvsubtopic->delete();
        return redirect('/admin/insert-mvsubsubtopic/');
    }
}
